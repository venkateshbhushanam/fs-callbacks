const fs = require("fs");
const path = require("path");

const randomFilesDirPath = path.join(__dirname, "./randomJsonFilesDir");

/*Using callbacks and the fs module's asynchronous functions, do the following:
 1. Create a directory of random JSON files
 2. Delete those files simultaneously */

function problem1() {

  let randomNum = Math.ceil(Math.random() * 10);
  let filesSuccessFullArray = [];
  let fileFailureArray = [];

  console.log("creating a directory of randomJsonFilesDir");

  fs.mkdir(randomFilesDirPath, { recursive: true }, (err, data) => {
    if (err) {
      console.error('Error: while creatiing a directory',err);
    } else {
      console.log("created randomJsonFilesDir directory");

      let emptyArray = new Array(randomNum).fill(0);

      emptyArray.map((eachItem, index) => {
        fs.appendFile(path.join(randomFilesDirPath, `./mynewfile1${index}.json`), JSON.stringify(`randomJSON${index}`),
          (err) => {
            if (err) {
              console.error('Error:',err);
              //storing files names which are failed to append 
              fileFailureArray.push(`mynewfile1${index}.json`);
            } else {
                //storing files names which are succesfully appended
              filesSuccessFullArray.push(`mynewfile${index}.json`);

              console.log(`creating file : mynewfile${index}.json`);
              if (
                filesSuccessFullArray.length + fileFailureArray.length ==
                randomNum
              ) {
                console.log("All files created");

                let filesDeletioncount = 0;
                //removing files by iterating on emptyArray
                emptyArray.map((eachItem, index) => {
                  fs.unlink(
                    path.join(randomFilesDirPath, `mynewfile1${index}.json`),
                    (err, data) => {
                      if (err) {
                        console.error("Error:error on deleting file",err);
                      } else {
                        console.log(`deleting file : mynewfile${index}.json`);
                        filesDeletioncount += 1;
                        if (filesDeletioncount == randomNum) {
                          console.log("all files deleted");
                          //deleting directory after deletion of all files
                          fs.rmdir(randomFilesDirPath, (err) => {
                            if (err) {
                              console.error('Error:',err);
                            } else {
                              console.log("directory removed");
                            }
                          });
                        }
                      }
                    }
                  );
                });
              }
            }
          }
        );
      });
    }
  });
}

module.exports = problem1;
