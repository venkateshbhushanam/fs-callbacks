const fs = require("fs");
const path = require("path");

const givenfilePath = path.join(__dirname, "./lipsum.txt");
const txtFilesNamesPath = path.join(__dirname, "./filenames.txt");

/*
Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

function problem2() {

  console.log("reading given file");
  fs.readFile(givenfilePath, "utf-8", (err, data) => {
    if (err) {
      console.error(err);
    } else {
      console.log("reading successfull");
      //storing the uppercase data into new file
      fs.writeFile(path.join(__dirname, "./upperCaseContent.txt"), data.toString().toUpperCase(), (err) => {
        if (err) {
          console.error(err);
        } else {
          console.log(`data stored in upperCaseContent.txt`);
          fs.writeFile(txtFilesNamesPath, "upperCaseContent.txt ", (err) => {
            if (err) {
              console.error(err);
            } else {
              console.log("upperCaseContent.txt file added in filesnames.txt");
              console.log("reading the upperCaseContent.txt");
              fs.readFile(path.join(__dirname, "./upperCaseContent.txt"), "utf-8", (err, data) => {
                if (err) {
                  console.error(err);
                } else {
                    //storing the data in the form of santances
                  let sentancedData = data.toString().toLowerCase().split(".").join("\n");

                  fs.writeFile(path.join(__dirname, "./sentanceddata.txt"), sentancedData, (err) => {
                    if (err) {
                      console.error(err);
                    } else {
                      console.log("created sentanceddata.txt");
                      fs.appendFile(txtFilesNamesPath, "sentanceddata.txt ", (err) => {
                        if (err) {
                          console.error(err);
                        } else {
                          console.log("sentanceddata.txt file added in filesNames.txt");
                          console.log("reading santanceddata.txt");
                          fs.readFile(path.join(__dirname, "./sentanceddata.txt"), "utf-8", (err, data) => {
                            if (err) {
                              console.error(err);
                            } else {
                              console.log("fetched data of santanceddata.tx");

                              let sentances = data.toString().split("\n");

                              let sortedSantancedData = sentances
                                .map((eachSantance) => {
                                  return eachSantance.trim();
                                })
                                .filter((eachSantance) => {
                                    //removing empty string
                                  if (eachSantance) {
                                    return true;
                                  } else {
                                    return false;
                                  }
                                })
                                .sort()
                                .join("\n");
                              console.log("sorted santanced data");
                              fs.writeFile(path.join(__dirname, "./sortedData.txt"), sortedSantancedData, (err) => {
                                if (err) {
                                  console.error(err);
                                } else {
                                  console.log("created sortedData.txt file");
                                  fs.appendFile(txtFilesNamesPath, "sortedData.txt ", (err) => {
                                    if (err) {
                                      console.error(err);
                                    } else {
                                      console.log("sorteData.txt file added in filesnames");
                                      fs.readFile(txtFilesNamesPath, "utf-8", (err, data) => {
                                        if (err) {
                                          console.error(err);
                                        } else {
                                          console.log("reading the contents of filenames.txt");
                                          let filenamesList = data
                                            .toString()
                                            .split(" ")
                                            .filter((eachItem) => {
                                              if (eachItem) {
                                                return true;
                                              } else {
                                                return false;
                                              }
                                            });

                                          filenamesList.map((eachFileName) => {
                                            fs.unlink(path.join(__dirname, eachFileName), (err) => {
                                              if (err) {
                                                console.error(err);
                                              } else {
                                                console.log(`deleting ${eachFileName}`);
                                              }
                                            });
                                          });
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = problem2;
